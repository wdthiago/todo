from flask import render_template, url_for, flash, redirect

from todo import app, db
from todo.forms import TaskForm
from todo.models import Task


@app.route('/')
def index():
    tasks = Task.query.all()
    return render_template('index.html', tasks=tasks)


@app.route('/new', methods=['GET', 'POST'])
def new():
    form = TaskForm()
    if form.validate_on_submit():

        task = Task()
        task.title = form.title.data
        task.description = form.description.data
        task.is_done = form.is_done.data

        db.session.add(task)
        db.session.commit()

        flash('Tarefa criada com sucesso.', 'success')
        return redirect(url_for('index'))

    return render_template('new.html', form=form)


@app.route("/<int:task_id>")
def details(task_id):
    task = Task.query.get(task_id)
    form = TaskForm(obj=task)
    return render_template('details.html', task=task, form=form)


@app.route("/<int:task_id>/update", methods=['GET', 'POST'])
def update(task_id):
    task = Task.query.get(task_id)
    form = TaskForm(obj=task)

    if form.validate_on_submit():

        task.title = form.title.data
        task.description = form.description.data
        task.is_done = form.is_done.data
        db.session.commit()

        flash('Tarefa alterada com sucesso.', 'success')
        return redirect(url_for('details', task_id=task.id))

    return render_template('update.html', task=task, form=form)


@app.route("/<int:task_id>/delete", methods=['GET', 'POST'])
def delete(task_id):
    task = Task.query.get(task_id)
    form = TaskForm(obj=task)

    if form.validate_on_submit():

        db.session.delete(task)
        db.session.commit()

        flash('Tarefa excluída com sucesso.', 'success')
        return redirect(url_for('index'))

    return render_template('delete.html', task=task, form=form)
