from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Length


class TaskForm(FlaskForm):
    title = StringField('Título', validators=[DataRequired(), Length(min=2, max=128)])
    description = TextAreaField('Descrição')
    is_done = BooleanField('Concluída?')
    submit = SubmitField('Salvar')
